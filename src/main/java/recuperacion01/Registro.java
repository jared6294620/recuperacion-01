/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package recuperacion01;

/**
 *
 * @author jared
 */
public abstract class Registro extends Vehiculo {
    private int numServicio;
    private String fechaServicio;
    private int tipoServicio;
    private Vehiculo vehiculo;
    private String descAutomovil;
    private float costoMantenimiento;

    public Registro(int numServicio, String fechaServicio, int tipoServicio, Vehiculo vehiculo, String descAutomovil, float costoMantenimiento, String serieVehiculo, int tipoMotor, String marca, String modelo) {
        super(serieVehiculo, tipoMotor, marca, modelo);
        this.numServicio = numServicio;
        this.fechaServicio = fechaServicio;
        this.tipoServicio = tipoServicio;
        this.vehiculo = vehiculo;
        this.descAutomovil = descAutomovil;
        this.costoMantenimiento = costoMantenimiento;
    }

    public int getNumServicio() {
        return numServicio;
    }

    public void setNumServicio(int numServicio) {
        this.numServicio = numServicio;
    }

    public String getFechaServicio() {
        return fechaServicio;
    }

    public void setFechaServicio(String fechaServicio) {
        this.fechaServicio = fechaServicio;
    }

    public int getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(int tipoServicio) {
        this.tipoServicio = tipoServicio;
    }

    public Vehiculo getVehiculo() {
        return vehiculo;
    }

    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }

    public String getDescAutomovil() {
        return descAutomovil;
    }

    public void setDescAutomovil(String descAutomovil) {
        this.descAutomovil = descAutomovil;
    }

    public float getCostoMantenimiento() {
        return costoMantenimiento;
    }

    public void setCostoMantenimiento(float costoMantenimiento) {
        this.costoMantenimiento = costoMantenimiento;
    }
    
    public void calcularTotal(){
        
    }
    
    public void calcularDescuento(){
        
    }
}
