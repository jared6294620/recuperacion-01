/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package recuperacion01;

/**
 *
 * @author jared
 */
public abstract class VehiculoCarga extends Vehiculo {
    private float numCargas;

    public VehiculoCarga(float numCargas, String serieVehiculo, int tipoMotor, String marca, String modelo) {
        super(serieVehiculo, tipoMotor, marca, modelo);
        this.numCargas = numCargas;
    }

    public float getNumCargas() {
        return numCargas;
    }

    public void setNumCargas(float numCargas) {
        this.numCargas = numCargas;
    }
}
