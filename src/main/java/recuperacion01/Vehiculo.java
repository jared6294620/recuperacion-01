/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package recuperacion01;

/**
 *
 * @author jared
 */
public class Vehiculo {
    protected String serieVehiculo;
    protected int tipoMotor;
    protected String marca;
    protected String modelo;

    public Vehiculo(String serieVehiculo, int tipoMotor, String marca, String modelo) {
        this.serieVehiculo = serieVehiculo;
        this.tipoMotor = tipoMotor;
        this.marca = marca;
        this.modelo = modelo;
    }

    public Vehiculo() {
        this.serieVehiculo = "";
        this.tipoMotor = 0;
        this.marca = "";
        this.modelo = "";
    }

    public String getSerieVehiculo() {
        return serieVehiculo;
    }

    public void setSerieVehiculo(String serieVehiculo) {
        this.serieVehiculo = serieVehiculo;
    }

    public int getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(int tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}
